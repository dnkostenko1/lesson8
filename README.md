# lesson8

Задание: сделать pipeline сборки Dockerfile и сохранения image в репозитории gitlab.

Выполнено.

Так же выполнена установка и настройка локального репозитория sonatype/nexus3.
В него помещен один из образов на локальном компьютере. Работы производились командами:

docker volume create --name nexus-data
docker run -d -p 8081:8081 -p 8082:8082 --name nexus -v nexus-data:/nexus-data sonatype/nexus3

docker login 127.0.0.1:8082

docker tag be5d9414c851 127.0.0.1:8082/repository/repo-docker/nexus3
docker push 127.0.0.1:8082/repository/repo-docker/nexus3

docker pull repository/repo-docker/nexus3:latest
